Rails.application.routes.draw do
  devise_for :users
  root 'static_pages#home'
  get :app, to: 'static_pages#app'

  namespace :api do
    resources :activities, only: :index
    resources :babies, only: :index do
      get :activity_logs
    end
    resources :activity_logs, only: %i[create update]
  end
  resources :activity_logs, only: :index
end
