class CompatibleType < ActiveRecord::Migration[5.2]
  def change
    change_column :activity_logs, :baby_id, :bigint
    change_column :activity_logs, :assistant_id, :bigint
    change_column :activity_logs, :activity_id, :bigint
  end
end
