class StatusOnActivityLog < ActiveRecord::Migration[5.2]
  def change
    add_column :activity_logs, :status, :integer
  end
end
