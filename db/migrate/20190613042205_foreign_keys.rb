class ForeignKeys < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key :activity_logs, :babies
    add_foreign_key :activity_logs, :assistants
    add_foreign_key :activity_logs, :activities
  end
end
