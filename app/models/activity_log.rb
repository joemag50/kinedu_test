# frozen_string_literal: true

# Activity Log
class ActivityLog < ApplicationRecord
  belongs_to :baby
  belongs_to :assistant
  belongs_to :activity

  validates :start_time, presence: true
  validate :stop_time_must_be_bigger, on: :update
  before_create :init_status
  before_update :update_duration

  enum status: %i[started finished]

  def stop_time_must_be_bigger
    return unless stop_time < start_time

    errors.add(:stop_time, 'stop time must be bigger than start time')
  end

  private

  def update_duration
    self.duration = (stop_time - start_time) / 60
    self.status = :finished
  end

  def init_status
    self.status = stop_time ? :finished : :started
  end
end
