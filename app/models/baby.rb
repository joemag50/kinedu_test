# frozen_string_literal: true

# Baby
class Baby < ApplicationRecord
  validates :name, :birthday, :mother_name, presence: true
  has_many :activity_logs

  def self.to_response
    order(:created_at).map do |baby|
      { id: baby.id, name: baby.name, months: baby.months,
        mother_name: baby.mother_name, father_name: baby.father_name,
        address: baby.address, phone: baby.phone }
    end
  end

  def activity_logs
    super.order(:created_at).map do |activity_log|
      {
        id: activity_log.id,
        baby_id: activity_log.baby_id,
        assistant_name: activity_log.assistant.name,
        start_time: activity_log.start_time,
        stop_time: activity_log.stop_time
      }
    end
  end

  def months
    date1 = Time.now
    date2 = birthday
    (date1.year - date2.year) * 12 + \
      date1.month - date2.month - \
      (date1.day >= date2.day ? 0 : 1)
  end
end
