# frozen_string_literal: true

# Activity
class Activity < ApplicationRecord
  validates :name, :description, presence: true
  has_many :activity_logs

  def self.to_response
    order(:created_at).select(:id, :name, :description)
  end
end
