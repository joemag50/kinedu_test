# frozen_string_literal: true

# Assistant
class Assistant < ApplicationRecord
  validates :name, presence: true
  has_many :activity_logs
end
