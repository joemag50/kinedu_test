# frozen_string_literal: true

# Activity Logs Controller
class ActivityLogsController < ApplicationController
  before_action :authenticate_user!

  def index
    set_activities
    return paginated unless params['activity_log']

    search_baby
    search_assistant
    search_status

    paginated
  end

  private

  def set_activities
    @activity_logs = ActivityLog.order(:start_time)
  end

  def search_baby
    return if object_params['baby_id'].nil?
    return if object_params['baby_id'].empty?

    @activity_logs = @activity_logs.where(baby_id: object_params['baby_id'])
  end

  def search_assistant
    return if object_params['assistant_id'].nil?
    return if object_params['assistant_id'].empty?

    @activity_logs =
      @activity_logs.where(assistant_id: object_params['assistant_id'])
  end

  def search_status
    return if object_params['status'].nil?
    return if object_params['status'].empty?

    @activity_logs = @activity_logs.where(status: object_params['status'])
  end

  def object_params
    params.require(:activity_log).permit(:baby_id, :assistant_id, :status)
  end

  def paginated
    @activity_logs = @activity_logs.paginate(page: params[:page])
  end
end
