# frozen_string_literal: true

# Application Controller
class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :render_404

  def render_404
    render json: { error: 'Not found', status: :not_found }, status: :not_found
  end
end
