# frozen_string_literal: true

module Api
  # Activity Logs Controller
  class ActivityLogsController < ApplicationController
    protect_from_forgery

    def create
      @activity_log = ActivityLog.new create_params
      if @activity_log.save
        render json: @activity_log, status: :ok
      else
        render json: { errors: @activity_log.errors },
               status: :unprocessable_entity
      end
    end

    def update
      find_activity_log
      return unless valid_date?

      if @activity_log.update update_params
        render json: @activity_log, status: :ok
      else
        render json: { errors: @activity_log.errors },
               status: :unprocessable_entity
      end
    end

    private

    def valid_date?
      return true unless update_params[:stop_time].empty?

      render json: { errors: { stop_time: 'Not valid date' } },
             status: :unprocessable_entity
      false
    end

    def find_activity_log
      @activity_log = ActivityLog.find(params[:id])
      return if @activity_log

      render json: { errors: 'Not found' }, status: :not_found
    end

    def create_params
      params.require(:activity_log).permit(:activity_id,
                                           :baby_id,
                                           :assistant_id,
                                           :start_time)
    end

    def update_params
      params.require(:activity_log).permit(:id, :stop_time, :comments)
    end
  end
end
