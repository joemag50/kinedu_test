# frozen_string_literal: true

module Api
  # Activities Controller
  class ActivitiesController < ApplicationController
    def index
      render json: Activity.to_response, status: :ok
    end
  end
end
