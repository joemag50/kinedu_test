# frozen_string_literal: true

module Api
  # Babies Controller
  class BabiesController < ApplicationController
    def index
      render json: Baby.to_response, status: :ok
    end

    def activity_logs
      find_baby
      render json: @baby.activity_logs, status: :ok
    end

    private

    def find_baby
      @baby = Baby.find(params[:baby_id])
    end
  end
end
